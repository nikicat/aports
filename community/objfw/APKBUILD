# Contributor: Jonathan Schleifer <js@nil.im>
# Maintainer: Jonathan Schleifer <js@nil.im>
pkgname=objfw
pkgver=1.1.1
pkgrel=0
pkgdesc="Portable, lightweight framework for the Objective-C language"
url="https://objfw.nil.im/"
arch="all"
license="LGPL-3.0-only"
makedepends="clang17 openssl-dev doxygen autoconf automake"
subpackages="$pkgname-dev $pkgname-doc libobjfw1:_libobjfw
	libobjfwrt1:_libobjfwrt libobjfwtls1:_libobjfwtls ofarc:_ofarc
	ofdns:_ofdns ofhash:_ofhash ofhttp:_ofhttp"
source="$pkgname-$pkgver.tar.gz::https://objfw.nil.im/downloads/objfw-$pkgver.tar.gz
	tr-workaround.patch"

prepare() {
	default_prepare
	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-rpath \
		OBJC="clang -target $CHOST"
	make
}

check() {
	make check
}

package() {
	depends="libobjfw1=$pkgver-r$pkgrel libobjfwrt1=$pkgver-r$pkgrel
		libobjfwtls1=$pkgver-r$pkgrel objfw-dev=$pkgver-r$pkgrel
		objfw-doc=$pkgver-r$pkgrel ofarc=$pkgver-r$pkgrel
		ofdns=$pkgver-r$pkgrel ofhash=$pkgver-r$pkgrel
		ofhttp=$pkgver-r$pkgrel"

	make DESTDIR="$pkgdir" install
}

dev() {
	amove usr/bin/objfw-compile
	amove usr/bin/objfw-config
	amove usr/bin/objfw-embed
	amove usr/bin/objfw-new
	amove usr/include
	amove usr/lib/*.so
	amove usr/lib/objfw-config
}

doc() {
	cd "$builddir"
	make docs
	mkdir -p "$subpkgdir"/usr/share/doc/objfw
	cp -r docs "$subpkgdir"/usr/share/doc/objfw/html
}

_libobjfw() {
	pkgdesc="Library needed by programs using ObjFW"

	amove usr/lib/libobjfw.so.*
}

_libobjfwrt() {
	pkgdesc="ObjFW Objective-C runtime library"

	amove usr/lib/libobjfwrt.so.*
}

_libobjfwtls() {
	pkgdesc="Library for TLS support for ObjFW"

	amove usr/lib/libobjfwtls.so.*
}

_ofarc() {
	pkgdesc="Utility for handling ZIP, Tar and LHA archives"

	amove usr/bin/ofarc
	amove usr/share/ofarc
}

_ofdns() {
	pkgdesc="Utility for performing DNS requests on the command line"

	amove usr/bin/ofdns
	amove usr/share/ofdns
}

_ofhash() {
	pkgdesc="Utility to hash files with various cryptographic hashes"

	amove usr/bin/ofhash
	amove usr/share/ofhash
}

_ofhttp() {
	pkgdesc="Command line downloader for HTTP(S)"

	amove usr/bin/ofhttp
	amove usr/share/ofhttp
}

sha512sums="
ee0e390e8fc6af8f98b9f91d3197bafad1612924c94b52f432e143dd58d84b0897c1a184466d3c3b18ef21334b02192f759aaa3c121d7e8949730574d012c2af  objfw-1.1.1.tar.gz
396ec11a05cee6023455fc1bc4c288fcb2ffe8008bdb40bb537c508e3f9d4e98c1e6b0728dc4db6644a8330829001766b101c99a1ad8ece2d6493c514d67e9f7  tr-workaround.patch
"

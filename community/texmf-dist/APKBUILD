# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Maintainer: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
pkgname=texmf-dist
pkgver=2024.0
_release=20240312
pkgrel=1
pkgdesc="TeX Live texmf core distribution"
url="https://tug.org/texlive/"
# texlive
arch="noarch !s390x !ppc64le !riscv64"
license="LPPL-1.0 LPPL-1.1 LPPL-1.2 LPPL-1.3a LPPL-1.3c GPL-2.0-only"
depends="
	perl-file-homedir
	perl-unicode-linebreak
	perl-yaml-tiny
	"
# using gawk speeds parsing the tlpdb up, reduces packaging time significantly.
makedepends="
	gawk
	"
subpackages="
	texmf-dist-lang
	texmf-dist-full
	texmf-dist-most

	texmf-dist-bibtexextra:_subpkg
	texmf-dist-binextra:_subpkg
	texmf-dist-context:_subpkg
	texmf-dist-fontsextra:_subpkg
	texmf-dist-fontsrecommended:_subpkg
	texmf-dist-fontutils:_subpkg
	texmf-dist-formatsextra:_subpkg
	texmf-dist-games:_subpkg
	texmf-dist-humanities:_subpkg
	texmf-dist-langarabic:_subpkg
	texmf-dist-langchinese:_subpkg
	texmf-dist-langcjk:_subpkg
	texmf-dist-langcyrillic:_subpkg
	texmf-dist-langczechslovak:_subpkg
	texmf-dist-langenglish:_subpkg
	texmf-dist-langeuropean:_subpkg
	texmf-dist-langfrench:_subpkg
	texmf-dist-langgerman:_subpkg
	texmf-dist-langgreek:_subpkg
	texmf-dist-langitalian:_subpkg
	texmf-dist-langjapanese:_subpkg
	texmf-dist-langkorean:_subpkg
	texmf-dist-langother:_subpkg
	texmf-dist-langpolish:_subpkg
	texmf-dist-langportuguese:_subpkg
	texmf-dist-langspanish:_subpkg
	texmf-dist-latex:_subpkg
	texmf-dist-latexextra:_subpkg
	texmf-dist-latexrecommended:_subpkg
	texmf-dist-luatex:_subpkg
	texmf-dist-mathscience:_subpkg
	texmf-dist-metapost:_subpkg
	texmf-dist-music:_subpkg
	texmf-dist-pictures:_subpkg
	texmf-dist-plaingeneric:_subpkg
	texmf-dist-pstricks:_subpkg
	texmf-dist-publishers:_subpkg
	texmf-dist-texworks:_subpkg
	texmf-dist-wintools:_subpkg
	texmf-dist-xetex:_subpkg
	"
source="
	https://ftp.tu-chemnitz.de/pub/tug/historic/systems/texlive/${_release:0:4}/texlive-$_release-texmf.tar.xz
	tlpkg-$pkgver.tar.gz::https://ftp.tu-chemnitz.de/pub/tug/historic/systems/texlive/${_release:0:4}/install-tl-unx.tar.gz
	texlive-$pkgver.tlpdb::https://git.texlive.info/texlive/plain/Master/tlpkg/texlive.tlpdb?h=tags/texlive-$pkgver

	0001-texmfcnf.patch
	0002-fix-newtt-map.patch
	"
builddir="$srcdir/texlive-$_release-texmf"

_tlpdb="$srcdir/texlive-$pkgver.tlpdb"

_tlpdb_get_values() {
	local pkg
	local key
	pkg="$1"
	key="$2"

	awk \
		"/^name $pkg\$/,/^$/{if(/^$key /) print substr(\$0, length(\"$key\") + 2)}" \
		"$_tlpdb"
}

_tlpdb_get_files() {
	local pkg
	pkg="$1"

	awk \
		"/^name $pkg\$/,/^$/{ if (/^ texmf-dist\/.*\$/) print \$1 }" \
		"$_tlpdb"
}

_tlpdb_get_collections() {
	awk \
		'/^name collection-([a-z0-9_-]+)$/{print substr($0, 17)}' \
		"$_tlpdb"
}

_pack_collection() {
	local collection
	local destdir
	local deps
	collection="$1"
	destdir="$2"
	deps="$(_tlpdb_get_values "collection-$collection" "depend")"


	for dep in $deps; do
		case "$dep" in
		collection-basic)
			# All subpackages already depend on texmf-dist
			;;
		collection-*)
			local actualdep
			actualdep="texmf-dist-${dep#collection-}"
			msg "Injecting dependency to $actualdep"
			depends="$depends $actualdep"
			;;
		*)
			msg "Adding $dep"
			local paths
			paths="$(_tlpdb_get_files "$dep")"
			for path in $paths; do
				mkdir -p "$destdir/usr/share/$(dirname "$path")"
				cp "$builddir/$path" "$destdir/usr/share/$path"
			done
			;;
		esac
	done
}

prepare() {
	default_prepare

	# Don't install a pre-compiled binaries. The source code is distributed
	# along the pre-compiled binaries for those who need them. There are few
	# offenders with niche use cases, such as bible typesetting.
	local bin_files
	bin_files="
		texmf-dist/doc/luatex/opbible/txs-gen/mod2tex
		"

	for bin_file in $bin_files; do
		local escaped_path
		escaped_path="${bin_file//\//\\/}"
		sed -e "/^ $escaped_path\$/d" \
			-i "$_tlpdb"
	done

	# Script files with a shebang should be executable. Fix this, where
	# needed.
	# shellcheck disable=SC3045 # Not caring about dash compatibility here
	find . \
		-type f \
		\( -name '*.sh' -o -name '*.py' -o -name '*.lua' -o -name '*.perl' \) \
		-print0 \
		| while IFS= read -r -d '' scriptfile; do

		if head -n 1 "$scriptfile" | grep -Eq '^#!/(|usr/)bin'; then
			chmod +x "$scriptfile"
		fi
	done
}

build() {
	# We don't really "build" things here
	return 0
}

check() {
	# there are no unit tests, but we check if the subpackages do still
	# match the list of collections in the tlpdb
	local collections
	collections="$(_tlpdb_get_collections)"

	for collection in $collections; do
		case "$collection" in
		basic)
			# no subpackage for basic, the basic collection goes into the root
			# package instead
			;;
		*)
			case "$subpackages" in
			*"texmf-dist-$collection"*)
				# collection in subpackages, everything fine
				;;
			*)
				echo "subpackage \"texmf-dist-$collection\" missing"
				return 1
			esac
			;;
		esac
	done
}

package() {
	_pack_collection "basic" "$pkgdir"

	# install tlpkg parts needed for texconfig
	mkdir -p "$pkgdir"/usr/share/tlpkg
	cp -r "$srcdir/install-tl-$_release/tlpkg/TeXLive" \
		"$pkgdir"/usr/share/tlpkg/
}

_subpkg() {
	local collection
	collection="${subpkgname#texmf-dist-}"
	pkgdesc="texmf-dist: $(_tlpdb_get_values "collection-$collection" shortdesc)"
	depends="texmf-dist"
	_pack_collection "$collection" "$subpkgdir"
}

most() {
	pkgdesc="TeX Live texmf distribution including most packages"
	# everything but texmf-dist-lang* and texmf-dist-fontsextra
	depends="$pkgname"
	local collections
	collections="$(_tlpdb_get_collections)"

	for collection in $collections; do
		case "$collection" in
		lang*)
			;;
		basic)
			;;
		fontsextra)
			;;
		*)
			depends="$depends texmf-dist-$collection"
			;;
		esac
	done
	mkdir -p "$subpkgdir"
}

lang() {
	pkgdesc="TeX Live texmf distribution: Additional languages"
	depends="$pkgname"
	local collections
	collections="$(_tlpdb_get_collections)"

	for collection in $collections; do
		case "$collection" in
		lang*)
			depends="$depends texmf-dist-$collection"
			;;
		esac
	done
	mkdir -p "$subpkgdir"
}

full() {
	pkgdesc="Full TeX Live texmf distribution"
	depends="$pkgname-most $pkgname-lang $pkgname-fontsextra"
	mkdir -p "$subpkgdir"
}


sha512sums="
32d65b59e8ee334c301e1d918d7e3c8fa7c9be1d0a367331a76c658082200cd95f7318ce857c192f08bd9c1fcc1b31d4350f4ae0b59ec493565711fb06148830  texlive-20240312-texmf.tar.xz
d86fcf4a4d6a42d8236aded827175d764835f54fa5eefcb74641b35e53fdbc439fdbf7ada3a1baa88c62c2a7b9d683099aecbb77ef694bf9df5fccca7d1277d6  tlpkg-2024.0.tar.gz
2b419f95abeb6a061aa4c8f4a3b480611fbd1ca5e0acf7e9176e53c64c3a6709df1a3771c7e898b4e24ddccfaa0e98e3455c442e28477a5028a74f6fd8d536c9  texlive-2024.0.tlpdb
f4a071332067c5fd598110106614a5b3d4a11708d86c5483822961b05a663e39498f99223e389f5dbb57b5eb4295730b65d022e479703499c970adc6272d3a68  0001-texmfcnf.patch
1599bbe462196b21ae6ffbdb0bb971ba69011126fa5f882f0c6795715f7cde1b394a7eebe442047fa8ef4387f357cd0ebb34684c1661a0cb76b9352620f5891e  0002-fix-newtt-map.patch
"
